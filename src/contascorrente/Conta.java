package contascorrente;

import java.math.BigDecimal;

/**
 *
 * @author Lucas Santos
 */
public class Conta implements IConta {
    private String agencia;
    private String numero;
    private BigDecimal saldo;

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }
    
    @Override
    public void realizarSaque(BigDecimal valor) {
        this.saldo.subtract(valor);
    }
    
    @Override
    public void realizarDeposito(BigDecimal valor) {
        this.saldo.add(valor);
    }
}
