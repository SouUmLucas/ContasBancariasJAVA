package contascorrente;

import java.math.BigDecimal;

/**
 *
 * @author Lucas Santos
 */
public interface IConta {
    public void realizarSaque(BigDecimal valor);
    public void realizarDeposito(BigDecimal valor);
}
