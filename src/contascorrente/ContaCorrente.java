package contascorrente;

import java.math.BigDecimal;

/**
 *
 * @author Lucas Santos
 */
public class ContaCorrente extends Conta {
    /**
     * @description Metodo sobrescrito: cada saque e retirada de extrato implica em uma taxa de 10 centavos
                    de reais. Saques maiores que 5.000 implicam em uma taxa de 1% sobre o saque.
     * @param valor - Valor em BigDecimal contendo a quantidade a ser retirada
     */
    @Override
    public void realizarSaque(BigDecimal valor) {
        if(valor.doubleValue() > new BigDecimal(5000).doubleValue()) {
            this.setSaldo(this.getSaldo().subtract(new BigDecimal(valor.doubleValue() + (valor.doubleValue() * 0.01))));
        } else {
            this.setSaldo(this.getSaldo().subtract(new BigDecimal(valor.doubleValue() + 0.10)));
        }
    }
}
